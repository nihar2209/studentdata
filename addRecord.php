<?php

	$query = "CREATE DATABASE IF NOT EXISTS students";
		$conn -> query($query);
		$query = "USE students";
		$conn -> query($query);
		$query = "CREATE TABLE IF NOT EXISTS studentdata
			(MISID INT , 
			FULL_NAME VARCHAR(30) NOT NULL,
			YEAR_OF_ADMISSION YEAR(4) ,
			FEES DECIMAL(9 , 3) , 
			CGPA FLOAT(4 , 2) ,
			PRIMARY KEY(MISID) )";
		$conn -> query($query);
		$query = "SELECT * FROM studentdata";
		$result = $conn -> query($query);
		$rows = $result -> fetch_all(MYSQLI_ASSOC);
		if(!count($rows)) {
			$stmt = $conn -> prepare("INSERT INTO studentdata(MISID, FULL_NAME , YEAR_OF_ADMISSION , FEES , CGPA) VALUES(? , ? , ? , ? , ?)" );
			$stmt -> bind_param("sssss" , $mis , $name , $year , $fees , $cgpa);
			$mis = 111603011;
			$name = "Asmita Chavan";
			$year = 2016;
			$fees = 43000;
			$cgpa = 7.9;
			$stmt -> execute();

			$mis = 111603012;
			$name = "Gaurav Deore";
			$year = 2016;
			$fees = 50000;
			$cgpa = 6.6;
			$stmt -> execute();

			$mis = 111603013;
			$name = "Niharika Deshmukh";
			$year = 2016;
			$fees = 80000;
			$cgpa = 7.1;
			$stmt -> execute();

			$stmt -> close();
		}
	
?>
